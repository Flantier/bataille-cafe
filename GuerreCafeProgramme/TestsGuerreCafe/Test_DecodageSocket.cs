﻿using GuerreCafeProgramme;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsGuerreCafe
{
    [TestClass]
    public class Test_DecodageSocket
    {
        [TestMethod]
        public void TestIfWork()
        {
            //ne marche pas car nécessite d'abord le lancement du serveur, et ensuite implementation de son IP/port ci-dessous)
            Fonctions.InitValeursCases("172.16.0.88", 1212); //init le tableau et utilise DecodageSocket

            Assert.AreEqual("mer", Carte.casesCartes[0, 0].Gettype()); //test si premiere case en 0,0 intiee
            Assert.AreEqual("neutre", Carte.casesCartes[1, 1].Gettype()); //test si premiere case en 0,0 intiee
            Assert.AreEqual("mer", Carte.casesCartes[9, 9].Gettype()); //test si derniere case en 9,9 initiee


        }
    }
}
