﻿using GuerreCafeProgramme;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsGuerreCafe
{
    [TestClass]
    public class Test_MajNbParcellesCases
    {
        [TestMethod]
        public void TestUp()
        {
            Carte.InitCases();
            Carte.casesCartes[0, 0] = new Case(7, "neutre", true, 0, 0, 2, 1); //haut
            Carte.casesCartes[1, 0] = new Case(0, "neutre", true, 1, 0, 2, -1); //bas
            Carte.casesCartes[0, 1] = new Case(8, "neutre", true, 0, 1, 2, 2); //aucun interet pour le test
            Carte.casesCartes[1, 1] = new Case(8, "neutre", true, 1, 1, 2, 2); //idem
            Fonctions.MajNbParcellesCases(Carte.casesCartes[1, 0], 1, 0);
            //test
            Assert.AreEqual(7, Carte.casesCartes[1, 0].GetNbParcelle());

        }

        [TestMethod]
        public void TestLeft()
        {
            Carte.InitCases();
            Carte.casesCartes[0, 0] = new Case(7, "neutre", true, 0, 0, 1, 2); //gauche
            Carte.casesCartes[0, 1] = new Case(0, "neutre", true, 0, 1, -1, 2); //droite
            Carte.casesCartes[1, 0] = new Case(8, "neutre", true, 1, 0, 2, 2); //aucun interet pour le test
            Carte.casesCartes[1, 1] = new Case(8, "neutre", true, 1, 1, 2, 2); //idem
            Fonctions.MajNbParcellesCases(Carte.casesCartes[0, 1], 0, 1);
            //test
            Assert.AreEqual(7, Carte.casesCartes[0, 1].GetNbParcelle());
        }

    }
}
