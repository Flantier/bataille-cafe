﻿using GuerreCafeProgramme;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsGuerreCafe
{
    [TestClass]
    public class Test_CreationDifferentsNbParcelles
    {
        [TestMethod]
        public void TestTrue1()
        {
            Carte.InitCases();
            Carte.casesCartes[0, 0] = new Case(-1, "neutre", true, 0, 0, 1, 2); //gauche
            Carte.casesCartes[0, 1] = new Case(-1, "neutre", true, 0, 1, -1, 2); //droite
            Fonctions.CreationDifferentsNbParcelles(Carte.casesCartes[0, 0]);
            Assert.AreEqual(0, Carte.casesCartes[0, 0].GetNbParcelle()); //premiere parcelle donc = 0


        }

        [TestMethod]
        public void TestTrue2()
        {
            Carte.casesCartes[1, 0] = new Case(-1, "neutre", true, 1, 0, 1, 2); //gauche
            Carte.casesCartes[1, 1] = new Case(-1, "neutre", true, 1, 1, -1, -1); //droite
            Carte.casesCartes[0, 1] = new Case(3, "neutre", true, 0, 1, 2, 1); //haut
            Fonctions.CreationDifferentsNbParcelles(Carte.casesCartes[1, 0]);
            Assert.AreNotEqual(1, Carte.casesCartes[1, 0].GetNbParcelle());


        }

    }
}
