﻿using GuerreCafeProgramme;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsGuerreCafe
{
    [TestClass]
    public class Test_DecodageDonneesCase
    {
        [TestMethod]
        public void TestMer_Nord_Sud()
        {
            string Mer_Nord_Sud = "69"; //64(mer) + 4(sud) + 1(nord)
            Case TestCase = new Case(0, "neutre", true, 0, 0, 0, 0);
            Fonctions.DecodageDonneesCase(Mer_Nord_Sud, TestCase);
            Assert.AreEqual(2, TestCase.GetFrontiereNS());
            Assert.AreEqual("mer", TestCase.Gettype());
        }

        [TestMethod]
        public void TestForet_Est_Ouest()
        {
            string Mer_Nord_Sud = "42"; //32(foret) + 2(ouest) + 8(est)
            Case TestCase = new Case(0, "neutre", true, 0, 0, 0, 0);
            Fonctions.DecodageDonneesCase(Mer_Nord_Sud, TestCase);
            Assert.AreEqual(2, TestCase.GetFrontiereEO());
            Assert.AreEqual("foret", TestCase.Gettype());
        }

        [TestMethod]
        public void TestNeutre_Nord_Ouest()
        {
            string Mer_Nord_Sud = "3"; //0(neutre) + 2(ouest) + 1(nord)
            Case TestCase = new Case(0, "neutre", true, 0, 0, 0, 0);
            Fonctions.DecodageDonneesCase(Mer_Nord_Sud, TestCase);
            Assert.AreEqual(1, TestCase.GetFrontiereNS());
            Assert.AreEqual(1, TestCase.GetFrontiereEO());
            Assert.AreEqual("neutre", TestCase.Gettype());
        }

        [TestMethod]
        public void TestNeutre_Sud_Est()
        {
            string Mer_Nord_Sud = "12"; //0(neutre)  + 4(sud) + 8(est)
            Case TestCase = new Case(0, "neutre", true, 0, 0, 0, 0);
            Fonctions.DecodageDonneesCase(Mer_Nord_Sud, TestCase);
            Assert.AreEqual(-1, TestCase.GetFrontiereNS());
            Assert.AreEqual(-1, TestCase.GetFrontiereEO());
            Assert.AreEqual("neutre", TestCase.Gettype());
        }

    }
}
