﻿using GuerreCafeProgramme;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TestsGuerreCafe
{
    [TestClass]
    public class Test_LiaisonCasesParcelle
    {
        [TestMethod]
        public void TestIfWork()
        {
            Carte.nbParcelles = 0;
            //ne marche pas car nécessite d'abord le lancement du serveur, et ensuite implementation de son IP/port ci-dessous)
            Fonctions.InitValeursCases("172.16.0.88", 1212);
            Fonctions.CasesParcellesCarte();
            Assert.AreEqual(17, Carte.nbParcelles); //car 17 parcelles neutres, mer et foret ne comptent pas

        }
    }
}
