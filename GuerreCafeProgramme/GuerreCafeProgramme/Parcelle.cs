﻿using System.Collections.Generic;

namespace GuerreCafeProgramme
{
    public class Parcelle //!!!!!! les parcelles d'eaux et de forets ne sont pas compris dans les 17 parcelles
    {
        private int nbParcelle; //numéro de la parcelle, de 0 à 16
        private string type; //un type commun à toutes ses cases (mer, foret ou normal)
        private int tailleParcelle;
        private List<Case> casesParcelle;
        private int parcellesPlantables;
        /* 4 parcelles de 6 cases
         * 5 parcelles de 4 cases
         * 4 parcelles de 3 cases
         * 4 parcelles de 2 cases
        */

        public Parcelle(int nbParcelle, string type, int tailleParcelle)
        {
            this.nbParcelle = nbParcelle;
            this.type = type;
            this.tailleParcelle = tailleParcelle;
            this.casesParcelle = new List<Case>();
            this.parcellesPlantables = 0;

        }

        //Getters
        public int GetNbParcelle()
        {
            return this.nbParcelle;
        }

        public string Gettype()
        {
            return this.type;
        }

        public int GetTailleParcelle()
        {
            return this.tailleParcelle;
        }

        public Case GetCasesParcelle(int num)
        {
            return casesParcelle[num];
        }

        public int GetParcellesPlantables()
        {
            return this.parcellesPlantables;
        }

        //Setters
        public void SetNbParcelle(int nbParcelle)
        {
            this.nbParcelle = nbParcelle;
        }
        public void Settype(string type)
        {
            this.type = type;
        }
        public void SetTailleParcelle(int tailleParcelle)
        {
            this.tailleParcelle = tailleParcelle;
        }
        public void SetCasesParcelle(Case caseParcelle)
        {
            this.casesParcelle.Add(caseParcelle);
        }
        public void SetParcellesPlantables(int parcellesPlantables)
        {
            this.parcellesPlantables = parcellesPlantables;
        }

        //Méthodes


    }
}
