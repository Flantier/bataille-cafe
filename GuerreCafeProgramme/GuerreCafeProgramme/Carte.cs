﻿using System.Collections.Generic;

namespace GuerreCafeProgramme
{
    public static class Carte
    {
        public static List<Parcelle> parcellesCarte = new List<Parcelle>(); //17 parcelles, numérotées de 0 a 16 pour simplifier l'index
        public static Case[,] casesCartes = new Case[10, 10];
        public static char[,] graineCartes = new char[10, 10]; //R = rien, O = graine a soi, N = graine ennemie
        public static int nbParcelles = 0;

        public static void InitCases()
        {
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    casesCartes[compteurX, compteurY] = new Case(0, "neutre", true, compteurX, compteurY, 0, 0);
                }
            }
        }


        public static void InitGraines()
        {
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    graineCartes[compteurX, compteurY] = 'R';
                }
            }
        }

        public static void InitParcelles()
        {
            for (int compteur = 0; compteur < nbParcelles; compteur++)
                parcellesCarte.Add(new Parcelle(compteur, "neutre", 0));
        }

    }
}
