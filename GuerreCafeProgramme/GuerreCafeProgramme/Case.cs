﻿namespace GuerreCafeProgramme
{
    public class Case
    {
        private int nbParcelle; //numéro de la parcelle, de 0 à 16
        private string type; //un type commun à toutes ses cases (mer, foret ou neutre)
        private bool plantable; //peut, sauf si mer ou fôret, être plantée
        private int positionX; //de gauche à droite
        private int positionY; //de haut en bas
        private int frontiereNS; //nord = 1, sud = -1, les deux = 2, aucun = 0
        private int frontiereEO; //est = -1, ouest = 1, les deux = 2; aucun = 0

        public Case(int nbParcelle, string type, bool plantable, int positionX, int positionY, int frontiereEO, int frontiereNS)
        {
            this.nbParcelle = nbParcelle;
            this.type = type;
            this.plantable = plantable;
            this.positionX = positionX;
            this.positionY = positionY;
            this.frontiereEO = frontiereEO;
            this.frontiereNS = frontiereNS;
        }

        //Getters
        public int GetNbParcelle()
        {
            return this.nbParcelle;
        }

        public string Gettype()
        {
            return this.type;
        }

        public bool GetPlantable()
        {
            return this.plantable;
        }

        public int GetPositionX()
        {
            return this.positionX;
        }

        public int GetPositionY()
        {
            return this.positionY;
        }

        public int GetFrontiereNS()
        {
            return this.frontiereNS;
        }

        public int GetFrontiereEO()
        {
            return this.frontiereEO;
        }

        //Setters
        public void SetNbParcelle(int nbParcelle)
        {
            this.nbParcelle = nbParcelle;
        }

        public void Settype(string type)
        {
            this.type = type;
        }

        public void SetPlantable(bool plantable)
        {
            this.plantable = plantable;
        }
        public void SetPositionX(int positionX)
        {
            this.positionX = positionX;
        }
        public void SetPositionY(int positionY)
        {
            this.positionY = positionY;
        }

        public void SetFrontiereNS(int frontiereNS)
        {
            this.frontiereNS = frontiereNS;
        }

        public void SetFrontiereEO(int frontiereEO)
        {
            this.frontiereEO = frontiereEO;
        }
        //Méthodes


    }
}
