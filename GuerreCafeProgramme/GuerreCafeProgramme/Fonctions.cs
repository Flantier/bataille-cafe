﻿using System;
using System.Net.Sockets; // pour les sockets
using System.Text;

namespace GuerreCafeProgramme
{
    public static class Fonctions
    {
        //Partie du semestre 3

        //la fonction socket

        public static Socket CreaSocket(string ip, int port)
        /* Permet l'initalisation d'une Socket selon des parametres specifiques
         * ip : l'ip du serveur dont on cherche la connexion
         * port : le port utilise pour la connexion
        */
        {
            Socket LaSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            LaSocket.Connect(ip, port);
            return LaSocket;
        }


        public static string RetourSocket(Socket LaSocket)
        /* Renvoie le code issu du serveur, connecte via une socket
         * LaSocket : la socket qui nous lie au serveur
         * bytesReceived : la variable qui obtiendra le flux de donnees sur le serveur
         * code : le formatage string de bytesReceived
        */
        {
            Byte[] bytesReceived = new Byte[256];
            int resultat = 0;
            string code = "";
            do
            {
                resultat = LaSocket.Receive(bytesReceived, bytesReceived.Length, 0);
                code = code + Encoding.ASCII.GetString(bytesReceived, 0, resultat);
                //Console.WriteLine(code); pour deboguer
            } while (resultat == 256);

            return code;
        }







        //du decodage a l'implementation des cases

        public static void DecodageDonneesCase(string aDecoder, Case caseChange)
        /* Permet, via un chiffre, de remplacer les valeurs de frontieres et de type d'une case
         * aDecoder : le chiffre, sous forme de string
         * puissance : la version int du chiffre
         * caseChange : la case concernee
        */
        {
            int puissance = int.Parse(aDecoder);

            if (puissance - (int)Math.Pow(2, 6) >= 0) //Mer
            {
                caseChange.Settype("mer");
                caseChange.SetPlantable(false);
                puissance = puissance - (int)Math.Pow(2, 6);
            }

            if (puissance - (int)Math.Pow(2, 5) >= 0) //Foret
            {
                caseChange.Settype("foret");
                caseChange.SetPlantable(false);
                puissance = puissance - (int)Math.Pow(2, 5);
            }

            if (puissance - (int)Math.Pow(2, 3) >= 0) //Est
            {
                caseChange.SetFrontiereEO(-1);
                puissance = puissance - (int)Math.Pow(2, 3);
            }

            if (puissance - (int)Math.Pow(2, 2) >= 0) //Sud
            {
                caseChange.SetFrontiereNS(-1);
                puissance = puissance - (int)Math.Pow(2, 2);
            }

            if (puissance - (int)Math.Pow(2, 1) >= 0) //Ouest
            {
                if (caseChange.GetFrontiereEO() == -1)
                {
                    caseChange.SetFrontiereEO(2);
                }
                else
                {
                    caseChange.SetFrontiereEO(1);
                }
                puissance = puissance - (int)Math.Pow(2, 1);
            }

            if (puissance - (int)Math.Pow(2, 0) >= 0) //Nord
            {
                if (caseChange.GetFrontiereNS() == -1)
                {
                    caseChange.SetFrontiereNS(2);
                }
                else
                {
                    caseChange.SetFrontiereNS(1);
                }
                puissance = puissance - (int)Math.Pow(2, 0);
            }
        }



        public static void CreationDifferentsNbParcelles(Case caseChange)
        /* Permet, dans un premier temps, d'assigner a chaque case en coin son numero de parcelle, en verifiant s'il s'agit bien d'une case en coin
         * caseChange : la case concernee
        */
        {
            if ((caseChange.GetFrontiereNS() == 1 | caseChange.GetFrontiereNS() == 2) & (caseChange.GetFrontiereEO() == 1 | caseChange.GetFrontiereEO() == 2)) //nord ou nord-sud, et ouest ou ouest-est
            {
                if (caseChange.Gettype() == "mer")
                {
                    caseChange.SetNbParcelle(64); //num des parcelles mers
                }
                if (caseChange.Gettype() == "foret")
                {
                    caseChange.SetNbParcelle(32); //num des parcelles forets
                }
                if (caseChange.Gettype() == "neutre")
                {
                    int positionX = caseChange.GetPositionX();
                    int positionY = caseChange.GetPositionY();

                    while ((Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 1 | Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 2) & (Carte.casesCartes[positionX, positionY].GetFrontiereEO() != -1))
                    {
                        positionY++;
                    }
                    if (Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 1 | Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 2)
                    {
                        caseChange.SetNbParcelle(Carte.nbParcelles);
                        Carte.nbParcelles++;

                    }
                    else
                    {
                        while (Carte.casesCartes[positionX, positionY].GetFrontiereNS() != 1)
                        {
                            positionX--;
                        }
                        caseChange.SetNbParcelle(Carte.casesCartes[positionX, positionY].GetNbParcelle());
                    }

                }

            }
        }


        public static void DecodageSocket(string code)
        /*Permet, via le string recupere via la socket, de recuperer les donnees necessaires a chaque case de la carte
         * code : le string recupere via la socket
         * compteurX : compteur des lignes du tableau
         * compteurY : compteur des colonnes du tableau
         * compteur : compteur permettant lire toute la ligne
         * codeCase : la code assigne a chaque case
         * compteurCode : int qui permet de se balader dans la variable 'code'
         * 
        */
        {
            int compteurX = 0, compteurY, compteurCode = 0;
            StringBuilder codeCase = new StringBuilder("");
            for (int compteur = 0; compteur < 10; compteur++) //pour affecter les 10 lignes de la carte
            {
                compteurY = -1;
                while (compteurX <= compteur) // pour être sur d'avoir lu une ligne en entière
                {
                    codeCase.Clear();
                    compteurY++; // pas 0 car represente l'index de la permiere case du tableau, donc causera un decalage dans les lignes du tableau
                    while ((code.Substring(compteurCode, 1) != ":") & (code.Substring(compteurCode, 1) != "|")) //s'arrête quand il y a un separateur
                    {
                        codeCase.Append(code.Substring(compteurCode, 1));
                        compteurCode++;
                    }
                    DecodageDonneesCase(codeCase.ToString(), Carte.casesCartes[compteurX, compteurY]);
                    //initialise les parcelles avec au moins les 'murs' nord-ouest
                    if (code.Substring(compteurCode, 1) == "|")
                    {
                        compteurX++;
                    }
                    compteurCode++;
                }
            }
        }

        public static void InitValeursCases(Socket laSocket)
        /* Initie les cases, et implemente les premieres valeurs, decodees via le string de la socket
         * code : le string recupere via la socket
         * LaSocket : La Socket en question
        */
        {
            string code = RetourSocket(laSocket);
            Carte.InitCases();
            DecodageSocket(code);
        }





        //creation des parcelles avec liaison a leurs cases, puis stockage dans la carte


        public static void MajNbParcellesCases(Case caseChange, int positionX, int positionY)
        /* Pour toutes les cases qui n'ont pas encore leur numero de parcelle, leur assigne
         * caseChange : la case concernee
         * positionX : la position X de la case dans le tableau
         * positionY : la position Y de la case dans le tableau
        */
        {
            if ((caseChange.GetFrontiereNS() == 1 | caseChange.GetFrontiereNS() == 2) & (caseChange.GetFrontiereEO() == 1 | caseChange.GetFrontiereEO() == 2)) //nord ou nord-sud, et ouest ou ouest-est
            {
                //rien car deja initialises
            }
            else
            {
                if (caseChange.GetFrontiereNS() == -1 | caseChange.GetFrontiereNS() == 0) //que sud ou aucun
                {
                    caseChange.SetNbParcelle(Carte.casesCartes[positionX - 1, positionY].GetNbParcelle()); //recup le nb de la case au dessus
                }
                if (caseChange.GetFrontiereEO() == -1 | caseChange.GetFrontiereEO() == 0) //que est ou aucun
                {
                    caseChange.SetNbParcelle(Carte.casesCartes[positionX, positionY - 1].GetNbParcelle()); //recup le nb de la case a gauche
                }
            }
        }


        public static void ReunionCaseNbParcelle()
        /* utilise la fonction 'MajNbParcellesCases' sur toutes les cases de la carte
         *
        */
        {
            //init nbparcelles
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    CreationDifferentsNbParcelles(Carte.casesCartes[compteurX, compteurY]);
                }
            }


            //maj nbParcelle
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    MajNbParcellesCases(Carte.casesCartes[compteurX, compteurY], compteurX, compteurY);
                }
            }

        }


        public static void LiaisonCasesParcelle()
        /* Lie les parcelle a leurs cases via le numero de parcelle
         * compteurPar : compteur de parcelles, par chacune les gerer via une boucle
         * compteurX : la position X de la case dans le tableau
         * compteurY : la position Y de la case dans le tableau 
        */
        {
            for (int compteurPar = 0; compteurPar < Carte.nbParcelles; compteurPar++)
            {
                for (int compteurX = 0; compteurX < 10; compteurX++)
                {
                    for (int compteurY = 0; compteurY < 10; compteurY++)
                    {
                        if (Carte.casesCartes[compteurX, compteurY].GetNbParcelle() == Carte.parcellesCarte[compteurPar].GetNbParcelle())
                        {
                            Carte.parcellesCarte[compteurPar].SetCasesParcelle(Carte.casesCartes[compteurX, compteurY]); //stocke la case dans sa parcelle
                            Carte.parcellesCarte[compteurPar].SetTailleParcelle(Carte.parcellesCarte[compteurPar].GetTailleParcelle() + 1); //incremente de 1 la taille de la parcelle
                        }
                    }
                }
                Carte.parcellesCarte[compteurPar].Settype(Carte.parcellesCarte[compteurPar].GetCasesParcelle(0).Gettype()); //recup le type de la premiere case
                if (Carte.parcellesCarte[compteurPar].Gettype() != "neutre")
                {
                    Carte.parcellesCarte[compteurPar].SetParcellesPlantables(Carte.parcellesCarte[compteurPar].GetTailleParcelle()); //le nombre de parcelle plantables sont égales aux nombre de cases

                }
            }
        }

        public static void CasesParcellesCarte()
        /* Assemble diverse fonctions pour assigner aux cases leur numero de parcelle, pour ensuite les lier a leur parcelle initialisees au prealable
        */
        {
            //donne aux dernieres cases leur numero de parcelle
            ReunionCaseNbParcelle();
            //initialisation parcelles
            Carte.InitParcelles();
            //liaison cartes et parcelles
            LiaisonCasesParcelle();
        }



        public static void AffichageCarte()
        {
            Console.WriteLine("Affichage de la carte : (le chiffre = nbParcelle; 64 = mer, 32 = foret)\n\n");
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    if (Carte.casesCartes[compteurX, compteurY].GetNbParcelle() < 10)
                    {
                        Console.Write("  " + Carte.casesCartes[compteurX, compteurY].GetNbParcelle()); //un espace en plus
                    }
                    else
                    {
                        Console.Write(" " + Carte.casesCartes[compteurX, compteurY].GetNbParcelle());
                    }
                    
                }
                Console.WriteLine("");
            }
        }









        //Partie du semestre 4



        //Dialogue avec le serveur

        //pour recuperer des donnees, une methode existe deja plus haut dans le code
        public static void EnvoieDonnees(Socket laSocket, string donnees)
        /* Permet au client d'envoyer des donnees au serveur
         * laSocket : socket qui permet de communiquer avec le serveur
         * donnees : les donnees a envoyer au serveur
         * msg : message configure pour etre envoye via un socket
        */
        {
            byte[] msg = Encoding.ASCII.GetBytes(donnees);
            laSocket.Send(msg);
        }

        

        //Mise en place de la stratégie

        public static void caseValableAdjacent(int tailleParcelle, ref bool found, ref string caseChoisie, int cooX, int cooY)
        /* Permet de trouver si une case permet d'obtenir des points grace a l'adjacence d'autres graines aux alentours
         * tailleParcelle: taille des parcelle visées
         * found : booleen qui permet de savoir si une case a ete trouvee
         * caseChoisie : coordonnees de la case potentiellement trouvee
         * cooX, cooY : coordonnees de la case actuellement verifiee
        */
        {
            int enHaut = cooX - 1;
            int enBas = cooX + 1;
            int aGauche = cooY - 1;
            int aDroite = cooY + 1;
            bool valide = false;
            int numParcelle = Carte.casesCartes[cooX, cooY].GetNbParcelle();

            foreach (Parcelle parcelle in Carte.parcellesCarte) //on cherche la parcelle liee au numero de parcelle de la case
            {
                if ((parcelle.GetTailleParcelle() == tailleParcelle) & (parcelle.GetNbParcelle() == numParcelle) & (!found)) //si bonne parcelle comme selon le parametre de taille et le num de la case
                {

                    if (enHaut >= 0) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[enHaut, cooY] == 'O')
                        {
                            valide = true;
                        }
                    }
                    if (enBas <= 9) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[enBas, cooY] == 'O')
                        {
                            valide = true;
                        }
                    }
                    if (aGauche >= 0) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[cooX, aGauche] == 'O')
                        {
                            valide = true;
                        }
                    }
                    if (aDroite <= 9) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[cooX, aDroite] == 'O')
                        {
                            valide = true;
                        }
                    }

                    if (valide) //si au moins une case adjacente
                    {
                        caseChoisie += cooX.ToString() + cooY.ToString();
                        found = true;
                    }

                }
            }

        }


        public static void caseValableParcelle(int tailleParcelle, ref bool found,ref string caseChoisie, int cooX, int cooY, ref string caseChoisieSecondeZone, ref string caseChoisieTroisiemeZone,ref string caseChoisiePasParChoix, bool reserves=false)
        /*Permet de choisir une case selon la parcelle et donc ses points potentiels
        * numParcelle : numero de parcelle de la case acutelle analysee
        * nbGrainesAdverse, nbGrainesJoueur, nbGrainesNulles : nombre de graines plantees dans une parcelle donnee, et a qui elle appartiennent
        * tailleParcelle : taille type des parcelles qu'on cherche
        * found : permet d'informer si une case pour la graine a ete trouvee
        * caseChoisie : les coordonnees d'une potentielle case choisie
        * caseChoisieSecondeZone, caseChoisieTroisiemeZone, caseChoisiePasParChoix : 'roues de secours' si notre stratageme ne reussi pas
        * reserves : ce qui met en place le plan B et les trois variables ci-dessus
        */
        {
            int nbGrainesJoueur = 0;
            int nbGrainesAdverse = 0;
            int nbGrainesNulles = 0;
            int numParcelle = Carte.casesCartes[cooX, cooY].GetNbParcelle();
            foreach (Parcelle parcelle in Carte.parcellesCarte) //on cherche la parcelle liee au numero de parcelle de la case
            {
                if ((parcelle.GetTailleParcelle() == tailleParcelle) & (parcelle.GetNbParcelle() == numParcelle) & (!found)) //si bonne parcelle comme selon le parametre de taille et le num de la case
                {
                    nbGrainesAdverse = 0;
                    nbGrainesJoueur = 0;
                    for (int compteurCase = 0; compteurCase < tailleParcelle; compteurCase++) //on verifie le nb de graines qu'on possede par rapport a l'adversaire dans cette parcelle
                    {
                        int cooXCase = parcelle.GetCasesParcelle(compteurCase).GetPositionX();
                        int cooYCase = parcelle.GetCasesParcelle(compteurCase).GetPositionY();
                        if (Carte.graineCartes[cooXCase, cooYCase] == 'O') { nbGrainesJoueur++; }
                        if (Carte.graineCartes[cooXCase, cooYCase] == 'N') { nbGrainesAdverse++; }
                        if (Carte.graineCartes[cooXCase, cooYCase] == 'R') { nbGrainesNulles++; }
                    }

                    if ((nbGrainesJoueur - nbGrainesAdverse == 0) & (tailleParcelle == 3) & (!found)) //si la parcelle a trois cases, et que deux sont remplies par chacun des joueurs, on peut recuperer l'avantage
                    {
                        caseChoisie += cooX.ToString() + cooY.ToString();
                        found = true;

                    }

                    if ((nbGrainesJoueur - nbGrainesAdverse == 1) & (!found)) //s'il n'y a qu'une graine d'ecart entre les deux joueurs
                    {
                        caseChoisie += cooX.ToString() + cooY.ToString();
                        found = true;
                    }

                    if ((nbGrainesNulles == tailleParcelle) & (!found)) //aucun joueur n'a encore pose dans la parcelle
                    {
                        caseChoisie += cooX.ToString() + cooY.ToString();
                        found = true;
                    }

                }
            }

            if (reserves)
            {
                //si pas de choix meilleur

                if ((nbGrainesJoueur == nbGrainesAdverse)) //les joueurs ont le meme nb de graines
                {
                    caseChoisieSecondeZone = "A:" + cooX.ToString() + cooY.ToString();
                }

                if ((nbGrainesJoueur - nbGrainesAdverse < 0)) //l'adversaire a plus de graines dans cette parcelle
                {
                    caseChoisieTroisiemeZone = "A:" + cooX.ToString() + cooY.ToString();
                }

                if (true) //toutes les cases sans graines
                {
                    caseChoisiePasParChoix = "A:" + cooX.ToString() + cooY.ToString();
                }

            }
        }


        public static void ChoixCase(int tailleParcelle, string cooDerniereGraine, ref bool found, ref string caseChoisie, ref string caseChoisieSecondeZone, ref string caseChoisieTroisiemeZone, ref string caseChoisiePasParChoix, bool reserves=false, bool adjacente=false)
        /*Va gerer le choix de case effectue par l'ordinateur
         * cooX et cooY : coordonnees de la derniere graine posee
         * compteurX et compteurY : compteurs permettant de traberser la carte du jeu
         * tailleParcelle : taille type des parcelles qu'on cherche
         * cooDerniereGraine : coordonnees de la derniere graine posee sous string
         * found : permet d'informer si une case pour la graine a ete trouvee
         * caseChoisie : les coordonnees d'une potentielle case choisie
         * caseChoisieSecondeZone, caseChoisieTroisiemeZone, caseChoisiePasParChoix : 'roues de secours' si notre stratageme ne reussi pas
         * reserves : ce qui met en place le plan B et les trois variables ci-dessus
         * adjacence : permet de savoir si on cherche a trouver les graines permettant d'obtenir des points par rapport aux parcelles ou a l'adjacence des autres graines
        */
        {
            int cooX = (int)Char.GetNumericValue(cooDerniereGraine[2]);
            int cooY = (int)Char.GetNumericValue(cooDerniereGraine[3]);

            for (int compteurY = 0; compteurY <= 9; compteurY++) //cherche horizontalement
            {
                if ((Carte.casesCartes[cooX, compteurY].GetPlantable()) & (!found)) //si la case est plantable
                {
                    if (!found) { caseValableParcelle(tailleParcelle, ref found, ref caseChoisie, cooX, compteurY, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, reserves); }
                    if ((!found) & (adjacente)) { caseValableAdjacent(tailleParcelle, ref found, ref caseChoisie, cooX, compteurY); }
                }
            }

            if (!found)
            {
                for (int compteurX = 0; compteurX <= 9; compteurX++) //cherche verticalement, donc idem mais verticalement
                {
                    if ((Carte.casesCartes[compteurX, cooY].GetPlantable()) & (!found)) //si la case est plantable
                    {
                        if (!found) { caseValableParcelle(tailleParcelle, ref found, ref caseChoisie, compteurX, cooY, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, reserves); }
                        if ((!found) & (adjacente)) { caseValableAdjacent(tailleParcelle, ref found, ref caseChoisie, compteurX, cooY); }
                    }
                }
            }

        }

        public static string Strategie(string aEviter, string cooDerniereGraine)
        /* Partie qui gere toute la strategie de l'I.A.
         * aEviter : coordonnees d'une case qui a pu generer une erreur 'INVA'
         * cooDerniereGraine : coordonnees de la derniere graine posee sous string
         * found : affirme si une case a ete trouvee pour poser une graine dessus
         * caseChoisie : coordonnees d'une potentielle case choisie pour poser une graine
         * caseChoisieSecondeZone, caseChoisieTroisiemeZone, caseChoisiePasParChoix : 'roues de secours' si notre stratageme ne reussi pas
        */
        {

            bool found = false;
            string caseChoisie = "A:";
            string caseChoisieSecondeZone = "A:";
            string caseChoisieTroisiemeZone = "A:";
            string caseChoisiePasParChoix = "A:";
            //on verifie d'abord selon les points obtenus par parcelles
            ChoixCase(3, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix);
            if (!found) { ChoixCase(6, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix); }
            if (!found) { ChoixCase(4, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix); }
            if (!found) { ChoixCase(2, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix); }
            //sinon on verifie selon les points obtenus par adjacence
            if (!found) { ChoixCase(3, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, false, true); }
            if (!found) { ChoixCase(6, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, false, true); }
            if (!found) { ChoixCase(4, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, false, true); }
            if (!found) { ChoixCase(2, cooDerniereGraine, ref found, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, true, true); }
            //sinon, on "fait avec ce qu'on a"
            if (!found) { caseChoisie = caseChoisieSecondeZone; }
            if (caseChoisie == "A:") { caseChoisie = caseChoisieTroisiemeZone; }
            if (caseChoisie == "A:") { caseChoisie = caseChoisiePasParChoix; }

            return caseChoisie;
        }

        public static void ChoixCaseJ1(int tailleParcelle, ref bool found, ref string caseChoisie)
            //meme fonctionnement que ChoixCase dans le principe mais pour le premier tour. Voir ChoixCase pour plus d'informations
        {
            int numParcelle = 99;
            for (int compteurX=0; compteurX<=9;compteurX++)
            {
                for (int compteurY = 0; compteurY<= 9; compteurY++)
                {
                    numParcelle = Carte.casesCartes[compteurX, compteurY].GetNbParcelle();
                    foreach (Parcelle parcelle in Carte.parcellesCarte) //on cherche la parcelle liee au numero de parcelle de la case
                    {
                        if ((parcelle.GetTailleParcelle() == tailleParcelle) & (parcelle.GetNbParcelle() == numParcelle) & (!found))
                        {
                            caseChoisie += compteurX.ToString() + compteurY.ToString();
                            found = true;

                        }
                    }

                }
            }
        }

        public static string Joueur1()
            //meme fonctionnement que Strategie dans le principe mais pour le premier tour. Voir Strategie pour plus d'informations
        {
            bool found = false;
            string caseChoisie = "A:";
            ChoixCaseJ1(3, ref found, ref caseChoisie);
            if (!found) { ChoixCaseJ1(6, ref found, ref caseChoisie); }
            if (!found) { ChoixCaseJ1(4, ref found, ref caseChoisie); }
            if (!found) { ChoixCaseJ1(2, ref found, ref caseChoisie); } //il y a au moins un des ces 4 types de parcelles

            return caseChoisie;

        }


        //Gerer les donnees de la socket

        public static void TraductionScoreFinal(string donneesSocket, ref int scoreJ1, ref int scoreJ2)
        /* Permet de recuperer le score des joueurs a partir des donnees recues de la socket
         * donneesSocket : donnees recues de la socket
         * scoreJ1, scoreJ2 : score des joueurs
        */
        {
            scoreJ1 = ((int)Char.GetNumericValue(donneesSocket[2]) * 10) + (int)Char.GetNumericValue(donneesSocket[3]);
            scoreJ2 = ((int)Char.GetNumericValue(donneesSocket[5]) * 10) + (int)Char.GetNumericValue(donneesSocket[6]);
        }


        public static int[] Jeu(Socket laSocket)
        {
            /* Oeuvre comme le coeur de notre I.A., en commmunicant et envoyant directement les choix de placement au serveur distant
             * laSocket : socket nous permettant de communiquer avec le serveur.
             * fin : permet de signaler a la boucle la fin de la partie.
             * donneesSocket : recupere toutes les donnees envoyees par la socket, des coordonnees aux messages speciaux.
             * choixPlacement : le choix de placement de notre I.A., envoye au serveur via la socket.
             * aEviter : dernier emplacement connu ayant cause une erreur 'INVA' de la part du serveur.
             * cooDerniereGraine : prend les donnees concernant la derniere graine plantee.
             * tour : sert a connaitre le numero de tour actuel.
             * tab : permet de renvoyer les donnees de resultats du jeu
            */
            bool fin = false;
            string donneesSocket = "";
            string choixPlacement = "";
            string aEviter = "";
            string cooDerniereGraine = "";
            int tour = 1;
            int scoreJ1 = 0, scoreJ2 = 0;

            int cooX = 0;
            int cooY = 0;

            while (!fin)
            {
                //le client joue
                if (tour==1) { choixPlacement = Joueur1(); }
                else { choixPlacement = Strategie(aEviter, cooDerniereGraine); }
                //Console.WriteLine(choixPlacement); pour deboguer
                EnvoieDonnees(laSocket, choixPlacement);


                //le client apprend si son coup a marche ou non
                donneesSocket = RetourSocket(laSocket);
                if (donneesSocket == "INVA") { aEviter = choixPlacement; }
                if (donneesSocket == "VALI")
                {
                    cooX = (int)Char.GetNumericValue(choixPlacement[2]);
                    cooY = (int)Char.GetNumericValue(choixPlacement[3]);
                    Carte.graineCartes[cooX, cooY] = 'O';
                    Carte.casesCartes[cooX, cooY].SetPlantable(false);
                    Console.WriteLine("Graine plantée en {0},{1} par le client !",cooX+1,cooY+1);
                }
                tour++; //tour du joueur


                //le client apprend si l'adversaire a pu jouer ou si la partie est finie
                donneesSocket = RetourSocket(laSocket);
                if (donneesSocket[0] == 'F')
                {
                    fin = true;
                    donneesSocket = RetourSocket(laSocket);
                    TraductionScoreFinal(donneesSocket, ref scoreJ1, ref scoreJ2);
                }
                if (donneesSocket[0] == 'B') 
                {
                    cooX = (int)Char.GetNumericValue(donneesSocket[2]);
                    cooY = (int)Char.GetNumericValue(donneesSocket[3]);
                    Carte.graineCartes[cooX, cooY] = 'N';
                    Carte.casesCartes[cooX, cooY].SetPlantable(false);
                    Console.WriteLine("Graine plantée en {0},{1} par le serveur !", cooX + 1, cooY + 1);
                    cooDerniereGraine = donneesSocket.Substring(0,4); //si pas que les donnees de coordonnees
                    tour++; //tour de l'adversaire

                    if (donneesSocket.Length > 4) //au cas ou les deux donnees B:xy et ENCO/FINI sont recuperees ensemble
                    {
                        //le client apprend s'il peut jouer ou si la partie est finie
                        if (donneesSocket[4] == 'E') { /*rien a faire*/ }

                        if (donneesSocket[4] == 'F')
                        {
                            fin = true;
                            donneesSocket = RetourSocket(laSocket);
                            TraductionScoreFinal(donneesSocket, ref scoreJ1, ref scoreJ2);
                        }
                    }

                    else
                    {
                        //le client apprend s'il peut jouer ou si la partie est finie
                        donneesSocket = RetourSocket(laSocket);
                        if (donneesSocket == "ENCO") { /*rien a faire*/ }
                        if (donneesSocket == "FINI")
                        {
                            fin = true;
                            donneesSocket = RetourSocket(laSocket);
                            TraductionScoreFinal(donneesSocket, ref scoreJ1, ref scoreJ2);
                        }
                    }

                   
                }
                

            } 

            //on ferme le serveur
            laSocket.Shutdown(SocketShutdown.Both);
            laSocket.Close();
            //on met de l'ordre dans les resultats pour les envoyer
            int[] tab = new int[] { -1, -1, -1}; //valeurs impossibles, donc efficace pour savoir s'il y a une erreur dans la reception des resultats
            tab[0] = tour; tab[1] = scoreJ1; tab[2] = scoreJ2;




            return tab;
        }


        public static void AffichageCarteGraines()
        //Permet d'afficher au joueur la carte une fois la partie finie
        {
            Console.WriteLine("Affichage de la carte finale : ( O = joueur, N = serveur, R = Personne)\n\n");
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    if (Carte.graineCartes[compteurX, compteurY] == 'O') { Console.ForegroundColor = ConsoleColor.Green; }
                    if (Carte.graineCartes[compteurX, compteurY] == 'N') { Console.ForegroundColor = ConsoleColor.Red; }
                    if (Carte.graineCartes[compteurX, compteurY] == 'R') { Console.ForegroundColor = ConsoleColor.Yellow; }
                    Console.Write(" " + Carte.graineCartes[compteurX, compteurY]);
                }
                Console.WriteLine("");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
