﻿using System;
using System.Net.Sockets; //pour les sockets
using System.Text; //pour le stringbuilder

namespace GuerreCafeProgramme
{
    class GuerreCafeMain
    {
        static void Main(string[] args)
        {
            //Semestre 3
            Console.WriteLine("Bienvenue à la bataille du café (veuillez appuyer pour lancer)");
            Console.ReadKey();

            //initialisation Socket
            Console.Write("\nInitialisation de la Socket :\nVeuillez donner l'adresse IP : ");
            string ip = Console.ReadLine();
            Console.Write("Veuillez donner le port : ");
            int port = int.Parse(Console.ReadLine());
            Carte.nbParcelles = 0;
            Socket laSocket = Fonctions.CreaSocket(ip,port);
            Console.WriteLine("Socket initialisée ");

            //initialisation carte
            Fonctions.InitValeursCases(laSocket);
            Console.WriteLine("Fin du décodage des données reçues par la Socket");

            Fonctions.CasesParcellesCarte(); //les donnes de la cartes sont initialisees
            Console.WriteLine("Initialisation de la carte depuis les données de la Socket terminée");

            //affichage
            Fonctions.AffichageCarte(); //fin du semestre 3 - la carte est affichee


            //Semestre 4

            Console.WriteLine("\nVeuillez appuyer pour commencer le jeu\n");
            Console.ReadKey();

            //initialisation de la carte des graines
            Carte.InitGraines();

            //jeu
            int[] resultat = Fonctions.Jeu(laSocket);

            //affichage des resultats
            StringBuilder final = new StringBuilder();
            Console.WriteLine(final.Append("\nLa partie s'est terminée en " + resultat[0] + " tours\nLe client a obtenu un score de " + resultat[1] + "\nLe serveur a obtenu un score de " + resultat[2]).ToString());
            Fonctions.AffichageCarteGraines();

            //fin
            Console.WriteLine("\nVeuillez appuyer pour terminer la partie");
            Console.ReadKey();


        }
    }
}
